import React, { FC } from 'react';

type TUser = {
    id: number
    name: string
}

const UsersPage: FC = async () => {
    const res = await fetch('https://jsonplaceholder.typicode.com/users');
    const users: TUser[] = await res.json();
    return (
        <div>
            <h1>Users</h1>
            <ul>
                {
                    users.map(user => <li key={user.id}>{ user.name }</li>)
                }
            </ul>
        </div>
    )
}

export default UsersPage;

UsersPage.displayName = 'UsersPage'
