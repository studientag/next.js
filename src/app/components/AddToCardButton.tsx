'use client';
import React, { FC } from 'react';

const AddToCardButton: FC = () => {
    return <button onClick={() => console.log('Okay')}>Zum Warenkorb hinzufügen</button>;
}

export default AddToCardButton;

AddToCardButton.displayName = 'AddToCardButton'
