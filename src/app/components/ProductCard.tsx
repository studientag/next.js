import React, { FC } from 'react';
import AddToCardButton from '@/app/components/AddToCardButton';

const ProductCard: FC = () => {
    return (
        <div>
            <h2>Produkt</h2>
            <AddToCardButton/>
        </div>
    )
}

export default ProductCard;

ProductCard.displayName = 'ProductCard'
