# Server Side Rendering + Next.JS

## Welche Arten von Rendering gibt es überhaupt?

### Client Side Rendering (CSR)

Beim Client Side Rendering enthält das HTML kaum mehr als die Notigen Header Tags und die nötigen Body Tags, es ist nur eine Hülle. Erst wenn das Javascript heruntergeladen wurde, beginnt dieses das HTML aufzufüllen und die Website erscheint erst dann.

![](https://content.dankbar.link/image/e91e6452-f6d9-4540-8577-ae80034a529f)

Frameworks:

* Angular.Js
* React.Js

### Server Side Rendering with Hydration (SSR)

Wenn man über SSR redet geht es meistens um SSR mit Javascript Hydration. Beim Intialen Laden der Website wird soviel am Server vorgerendet wie es geht und das resultierende HTML wird zum Client zurück gesendet. Neben dem HTML wird aber auch Javascript Code für das \_Hydration \_zurück geschickt. Der Browser kann das HTML bereits anzeigen, während das Javascript heruntergeladen und verarbeitet wird. Sobald es bereit ist übernimmt die Hydration ab den Zeitpunkt und alles weitere wird dann am Client gerendert.

![](https://content.dankbar.link/image/327a06d4-4d58-4dbe-bede-ec216433ca39)

Frameworks:

* Next.Js
* Nuxt.Js
* Sveltekit

### Vergleich

Bei CSR muss zuerst das JS herunterladen werden um auf der Website etwas darzustellen und bei SSR kann der Browser schoneinmal die HTML Seite darstellen und im Hintergrund das JS herunterladen und verarbeiten. Dies ist ein großer Vorteil für SEO da viele Crawler die Website aufrufen mit Javascript Deaktiviert.

Bei CSR reicht auch ein einfacher Webspace da nur die Datei zu verfügung gestellt werden. Bei SSR muss der Server richtig das HTML berechnen und dafür reicht ein einfacher Webspace nicht aus. Man kann sich das besser vorstellen, wenn man bedenkt das z.B.: Next.Js einfach nur ein Backend ist, welches HTML anstatt von JSON zurück gibt.

# NextJs Projekt

Für diesen Studientag findest du das Projekt unter: <https://gitlab.com/studientag/next.js>

Für das Projekt benutzte ich Next.JS 13.

Ab der 13. Version gibt es nun zwei Möglichkeiten die App aufzubauen. Über den Pages Router und über den App Router. Der App Router ist neu und auf den werde ich meine Test App aufbauen

## Routing

Das Routing in der App wird anhand der Dateistruktur festgelegt.

![](https://content.dankbar.link/image/bcaadb19-dd91-4b98-87f4-6224848b619b)

Für dieses Test Projekt möchte ich eine weitere Seite für \_users \_aufbauen. Dafür erstelle ich einen users Ordner mit einer page.tsx Datei. Die page.tsx ist wichtig und ein fest geschriebener Name von NextJs. In der page Datei müssen wir ein Component Exportieren welches auf der Seite gerendert werden soll.

Mit der page.tsx Datei unter src/app/users geschrieben können wir nun im Browser auf dieses Seite navigieren:

<img src="https://content.dankbar.link/image/20faf0bf-7178-4235-84fe-1317f85470e4" width="539" height="null" />

Und tada es hat Funktioniert. 😊

### Nested Route

Ich möchte nun eine weitere Route anlegen, nur soll es dieses mal eine Nested Route sein. Unter _users/new_ soll nun eine andere Seite angezeigt werden. Dafür erstelle ich im _users __\___Ordner einen neuen Ordner namens __\___new __\___und dortdrin die __\___page.tsx._

![](https://content.dankbar.link/image/149627d5-06c2-4cf5-8d70-0cc2f7d1f88d)

Im Browser kann ich nun zu _users/new_ navigieren und sehen, dass meine neue Seite gerendert wird.

<img src="https://content.dankbar.link/image/72cd1ef8-57ff-40d6-9dee-686153482e94" width="619" height="null" />

## Navigation

Womöglich wollen wir es den normalen Nutzer auch ermöglichen auf diese Spannenden anderen Seiten zu navigieren.

Entweder können wir einen Anchor Tag benutzen um auf die \_users \_Seite zu kommen, doch dass sollten wir nicht, da der Browser sonst alles nochmal Downloaded. In NextJs gibt es für die Navigation das **Link **Component.

<img src="https://content.dankbar.link/image/5381496c-70e8-44d5-ab01-a0caf9b31ebd" width="416" height="null" />

## Render Umgebungen

Wie oben Beschrieben haben wir zwei Umgebungen wo wir Sachen Render lassen können: Am Client (mittels JS Hydration) oder am Server.

Server Komponent haben Einschränkungen welche wir beachten müssen:

* Keine Browser Events (OnClick, OnChange, OnHover, ...)
* Keine Browser APIs (Local Storage)
* Keine Use Effects
* Keine States

Wegen diesen Einschränkungen sollte man eine Mischung aus Server und Client Komponenten benutzen.

### Komponenten erstellen

Alle Komponenten im _src/app_ Ordner sind standard mäßig Server Komponenten. In dem Ordner erstellen wir einen neuen Ordner namens _components_, dieser Ordner ist nicht erreichbar, da wir keine page.tsx anlegen. In diesen Ordner erstellen wir unsere _ProductCard.tsx_ Komponente.

In dieser Komponte wollen wir einen Button haben, der das Produkt in den Warenkorb legt, aber wir dürfen nicht vergessen, dass die Komponente standardmäßig als Server Komponente gerendert wird und diese dürfen keine Interaktion haben, deswegen bekommen wir auch ein Fehler wenn wir eine OnClick Funktion hinzufügen:

![](https://content.dankbar.link/image/5cfad8e8-527f-4f2d-a4d4-d149eb948098)

Um den Next JS Kompiler nun mitzuteilen, dass diese Komponente am Client rendert werden soll, fügen wir ob an der **ProductCard.tsx** Datei einfach **'use client'** an. Wenn andere Komponente eine Abhänigkeit zu dieser Komponente haben, werden die anderen auch Automatisch zu Client Komponenten. Da wir aber möglichst viel am Server rendern wollen sollten wir nur eine kleine Komponente Bauen die wir am Client Rendern, beispielweise würden wir den Button in eine _AddToCart.tsx_ Datei auslagern und nur diese am Client rendern lassen.

## Data Fetching

Data Fetching können wir in Server Komponenten machen. Wir benutzen einfach Fetch direkt in den Komponenten.

<img width="643" src="https://content.dankbar.link/image/da4e76c8-c352-4c3c-8658-af27612e2cce" />

Das Ergebnis der fetch Funktion wird gecached. Wenn wir das nicht wollen müssen wir bei dem zweiten Argument { cache: 'no-cache'} mitgeben.



## Static & Dynamic Rendering

Wenn wir Seiten oder Komponenten  mit Statischen Daten haben, können wir die mittels Static Rendering rendern. Diese Render Methode passiert beim builden und wir rendern die Komponente einmal vor, speichern diese als Datei und wenn gebraucht wird können wir sie ganz einfach auslesen und ausliefern.

Beim Dynamic Rendering wird die Komponente beim Request gerendert.

Ob eine Komponente Statisch oder Dynamisch gerendert wird, entscheided NextJs wenn es Denkt, dass sich die Daten ändern könnten. In unseren Data Fetching Beispiel wird das Ergebnis von der fetch Funktion in den Cache gespeichert und darum wird sich die Komponente auch nicht mehr danach ändern und Next wird diese als Statische Komponente rendern. Wenn wir der fetch Funktion mitteilen, dass diese keinen Cache benutzen soll, wird die Komponente dynamisch.



# Schlusswort

So erstellt man eine einfache Next JS Anwendungen. Der nächste Teil wäre Styling mit TailwindCss aber ich denke das würde den Rahmen komplett sprengen.



## Quellen

1. Next js Tutorial for Beginners | Nextjs 13 (App Router) with TypeScript | <https://www.youtube.com/watch?v=ZVnjOPwW4ZA>
2. 10 Rendering Patterns for Web Apps | <https://www.youtube.com/watch?v=Dkx5ydvtpCA> 


